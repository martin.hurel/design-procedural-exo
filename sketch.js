let bgColor;
let s;
let shape;

function setup(){
    s = {};
    // Permet d'avoir une forme avec une taille de base de 25
    s.ellipseWidth = 25;

    // Créer un canva
    createCanvas(600, 600);

    // Créer un background d'une couleur aléatoire
    background(random(255), random(255), random(255));

    // Définis la forme par défault étant un carré 
    shape = 'rect';
    
    let gui = new dat.GUI();
    gui.add(s, 'ellipseWidth', 0, 100).name('taille de la forme');
}    

function draw() {
    // Enleve les bordures de la forme
    noStroke();

    // Remplis la forme d'une couleur aléatoire
    fill(random(255), random(255), random(255));
}

function mouseClicked() {
    // Créer une forme à la position du click en fonction de la forme
    if(shape === 'rect'){
        rect(mouseX, mouseY, s.ellipseWidth, s.ellipseWidth);
    }
    else if(shape === 'ellipse'){
        ellipse(mouseX, mouseY, s.ellipseWidth, s.ellipseWidth);
    }
    // prevent default
    return false;
  }

function keyTyped(){
    if (key === 'f'){
        // Permet de changer la forme si l'utilisateur appuie sur "f"
        if(shape === 'rect'){
            shape = 'ellipse';
        }
        else if (shape === 'ellipse'){
            shape = 'rect';
        }
    }
    else if (key === 's'){
        //Permet d'enregistrer le canva avec la date du jour
        var today = new Date();
        var dd = String(today.getDate()).padStart(2, '0');
        var mm = String(today.getMonth() + 1).padStart(2, '0'); 
        var yyyy = today.getFullYear();
        today = mm + '-' + dd + '-' + yyyy;
        saveCanvas('mySketch-'+ today + '.jpg');
    }
}